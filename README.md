# Ansible collections example

## Commands
### Installing required collections
~~~bash
ansible-galaxy install -r collections/requirements.yml
~~~

### Make your own collection with
~~~bash
~~~

### Sources
- [Hands on with Ansible Collections](https://www.ansible.com/blog/hands-on-with-ansible-collections/)